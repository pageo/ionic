export class UserModel {
    // ----- Properties
    public fullName: string;
    public login: string;
    public email: string;    
    public password: string;
}