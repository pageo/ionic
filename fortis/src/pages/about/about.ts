import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  // ----- Constructors
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  // ----- Public methods
  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
}
