import { NavController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import * as AppConfig from '../../app/config';

export class ProtectedPage {

  // ----- Fields
  private cfg: any;

  // ----- Constructors
  constructor(
    public navCtrl: NavController,
    public storage: Storage) {
    this.cfg = AppConfig.cfg;
  }

  // ----- Override methods
  ionViewCanEnter() {
    this.storage.get(this.cfg.tokenName).then(token => {
      if (token === null) {
        this.navCtrl.setRoot('TutorialPage');
        return false;
      }
    });
    return true;
  }
} 
