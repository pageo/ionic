import { Component } from '@angular/core';

import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  // ----- Fields
  videosRoot = 'VideosPage';
  picturesRoot = 'PicturesPage';
  mapRoot = 'MapPage';
  aboutRoot = 'AboutPage';

  // ----- Properties
  mySelectedIndex: number;  

  // ----- Constructors
  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;    
  }
}
