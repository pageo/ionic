import { Component } from '@angular/core';

import { Storage } from '@ionic/storage';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FortisService } from "../../services/fortis-service";

import { ProtectedPage } from "../protected-page/protected-page";

@IonicPage()
@Component({
  selector: 'page-pictures',
  templateUrl: 'pictures.html'
})
export class PicturesPage extends ProtectedPage {
  
  // ----- Fields
  public feeds: any;
  
  // ----- Constructors
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public fortisService: FortisService) {
    super(navCtrl, storage);
  }

  // ----- Override methods
  ionViewWillEnter() {
    this.fortisService.getFeeds().then(feeds => this.feeds = feeds);
  }
}
