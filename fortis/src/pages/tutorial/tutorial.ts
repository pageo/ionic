import { Component, ViewChild } from '@angular/core';

import { IonicPage, Slides, MenuController, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {

  // ----- Fields
  showSkip = true;  

  // ----- Properties
  @ViewChild('slides') slides: Slides;  

  // ----- Constructors
  constructor(
    public navCtrl: NavController, 
    public menu: MenuController,    
    public navParams: NavParams,
    public storage: Storage    
  ) { }

  // ----- Public methods
  startApp() {
    this.navCtrl.push(TabsPage).then(() => {
      this.storage.set('hasSeenTutorial', 'true');
    })
  }

  onSlideChangeStart(slider: Slides) {
    this.showSkip = !slider.isEnd();
  }

  // ----- Override methods
  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

  ionViewWillEnter() {
		this.slides.update();
	}

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    this.menu.enable(true);
  }
}