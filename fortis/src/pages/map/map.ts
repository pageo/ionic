import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  // ----- Constructors
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  // ----- Public methods
  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

}
