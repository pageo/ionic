import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html'
})
export class VideosPage {

  // ----- Constructors
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  // ----- Public methods
  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }
}
