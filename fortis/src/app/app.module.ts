import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { IonicStorageModule, Storage } from '@ionic/storage';

import { FortisApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { FortisData } from '../providers/fortis-data';
import { UserData } from '../providers/user-data';

import * as AppConfig from '../app/config';

import { TutorialModule } from "../pages/tutorial/tutorial.module";
import { TabsModule } from "../pages/tabs/tabs.module";
import { AccountModule } from "../pages/account/account.module";

import { FortisService } from '../services/fortis-service';
import { AuthService } from '../services/auth-service';

import { AuthHttp } from "../utils/auth-http";

let storage = new Storage(null);
storage.set(AppConfig.cfg.tokenName, 'aWQ6MTtjcmVhdGlvbkRhdGU6MjMvMDgvMjAxNyAwMTozNTozNw==');

@NgModule({
  declarations: [
    FortisApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(FortisApp),
    IonicStorageModule.forRoot(),
    // Voir c'est toujours utile d'ajouter les pages modules ici
    TutorialModule,
    TabsModule,
    AccountModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    FortisApp
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    StatusBar,
    SplashScreen,
    InAppBrowser,
    AuthHttp,
    UserData,
    FortisData,
    FortisService,
    AuthService
  ]
})

export class AppModule { }