import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, Nav, Platform } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';

import { TabsPage } from "../pages/tabs/tabs";
import { VideosPage } from "../pages/videos/videos";
import { PicturesPage } from "../pages/pictures/pictures";
import { MapPage } from "../pages/map/map";
import { AboutPage } from "../pages/about/about";

import { FortisData } from '../providers/fortis-data';
import { UserData } from '../providers/user-data';
import { TutorialPage } from "../pages/tutorial/tutorial";

export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.html'
})
export class FortisApp {

  // ----- Properties
  @ViewChild(Nav) nav: Nav;
  appPages: PageInterface[] = [
    { title: 'Videos', name: 'TabsPage', component: TabsPage, tabComponent: VideosPage, index: 0, icon: 'film' },
    { title: 'Pictures', name: 'TabsPage', component: TabsPage, tabComponent: PicturesPage, index: 1, icon: 'images' },
    { title: 'Map', name: 'TabsPage', component: TabsPage, tabComponent: MapPage, index: 2, icon: 'map' },
    { title: 'About', name: 'TabsPage', component: TabsPage, tabComponent: AboutPage, index: 3, icon: 'information-circle' }
  ];
  rootPage: any;

  // ----- Constructors
  constructor(
    public events: Events,
    public fortisData: FortisData,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public storage: Storage,
    public splashScreen: SplashScreen
  ) {

    // Check if the user has already seen the tutorial
    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial) => {
        if (hasSeenTutorial)
          this.nav.setRoot('TabsPage');
        else
          this.nav.setRoot('TutorialPage');
        this.platformReady();
      });

    // load the fortis data
    //fortisData.load();

    this.userData.hasLoggedIn().then((hasLoggedIn) => this.enableMenu(hasLoggedIn === true));
    this.enableMenu(true);
    this.listenToLoginEvents();
  }

  // ----- Public methods
  openPage(page: PageInterface) {
    let params = {};

    if (page.index)
      params = { tabIndex: page.index };

    if (this.nav.getActiveChildNavs().length && page.index != undefined)
      this.nav.getActiveChildNavs()[0].select(page.index);
    else 
      this.nav.setRoot(page.name, params).catch((err: any) => console.log(`Didn't set nav root: ${err}`));
  
    if (page.logsOut)
      this.userData.logout();
  }

  openTutorial() {
    this.nav.setRoot(TutorialPage);
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => this.enableMenu(true));
    this.events.subscribe('user:signup', () => this.enableMenu(true));
    this.events.subscribe('user:logout', () => this.enableMenu(false));
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  platformReady() {
    this.platform.ready().then(() => this.splashScreen.hide());
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent)
        return 'primary';
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.name)
      return 'primary';
    return;
  }
}
