export let cfg = {
    tokenName: 'token',
    user: {
        authentificate: '/api/v1/authenticate',
        register: '/api/v1/register',
        info: '/api/v1/user',
    },
    feeds: '/api/v1/feeds',
    animes: '/api/v1/animes'
};