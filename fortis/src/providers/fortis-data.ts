import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import { UserData } from './user-data';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class FortisData {

  // ----- Fields
  data: any;

  // ----- Constructors
  constructor(
    public http: Http, 
    public user: UserData) { }
}