import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import { Storage } from '@ionic/storage';

import * as AppConfig from '../app/config';

import 'rxjs/add/operator/toPromise';

import { UserModel } from "../models/user.model";
import { CredentialsModel } from "../models/credentials-model";

@Injectable()
export class AuthService {

  // ----- Fields
  private _cfg: any;
  private _token: string;
  private _endPointUrl: String = "http://localhost/Fortis.CommonHost.Core";

  // ----- Constructors
  constructor(
    private _storage: Storage,
    public http: Http) {
    this._cfg = AppConfig.cfg;
    this._storage
      .get(this._cfg.tokenName)
      .then(token => {
        this._token = token;
      });
  }

  // ----- Public methods
  register(userData: UserModel) {
    return this.http.post(this._endPointUrl + this._cfg.user.register, userData)
      .toPromise()
      .then(data => {
        this.saveData(data);        
        let rs = data.json();
        this._token = rs.token;
      })
      .catch(e => console.log("reg error", e));
  }

  login(credentials: CredentialsModel) {
    return this.http.post(this._endPointUrl + this._cfg.user.login, credentials)
      .toPromise()
      .then(data => {
        this.saveData(data);        
        let rs = data.json();
        this._token = rs.token;
      })
      .catch(e => console.log('login error', e));
  }

  // ----- Internal logics
  saveData(data: any) {
    let rs = data.json();
    this._storage.set(this._cfg.tokenName, rs.token);
  }

  logout() {
    this._storage.remove(this._cfg.tokenName);
  }
}
