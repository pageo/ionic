import { Injectable } from '@angular/core';

import * as AppConfig from '../app/config';

import 'rxjs/add/operator/toPromise';

import { AuthHttp } from "../utils/auth-http";

@Injectable()
export class FortisService {

  // ----- Fields
  private cfg: any;
  private endPointUrl: String = "http://192.168.0.15/Fortis.CommonHost.Core";

  // ----- Constructors
  constructor(
    private authHttp: AuthHttp) {
    this.cfg = AppConfig.cfg;
  }

  // ----- Internal logics
  async getFeeds() {
    return await this.authHttp.get(this.endPointUrl + this.cfg.feeds)
      .toPromise()
      .then(res => res)
      .catch(e => console.log("feeds error", e));
  }
}
